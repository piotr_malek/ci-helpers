param(
    [string]$PackagePath,
    [string]$AppPoolName, 
    [string]$WebSiteName,
    [string[]]$BindingProtocols,
    [int[]]$BindingPorts
    )

$IISAppsRootDirectory = "E:\IIS apps\"

Import-Module WebAdministration
Add-Type -AssemblyName System.IO.Compression.FileSystem

# ----------------------------------------------------------
# Helpers
# ----------------------------------------------------------
function Execute-WithRetry([ScriptBlock] $command) {
    $attemptCount = 0
    $operationIncomplete = $true
    $maxFailures = 5

    while ($operationIncomplete -and $attemptCount -lt $maxFailures) {
        $attemptCount = ($attemptCount + 1)

        if ($attemptCount -ge 2) {
            Write-Host "Waiting for $sleepBetweenFailures seconds before retrying..."
            Start-Sleep -s $sleepBetweenFailures
            Write-Host "Retrying..."
        }

        try {
            # Call the script block
            & $command

            $operationIncomplete = $false
        } catch [System.Exception] {
            if ($attemptCount -lt ($maxFailures)) {
                Write-Host ("Attempt $attemptCount of $maxFailures failed: " + $_.Exception.Message)
            } else {
                throw
            }
        }
    }
}

# ----------------------------------------------------------
# Params validation
# ----------------------------------------------------------

if($BindingProtocols.Count -ne $BindingPorts.Count){
    Write-Error "Check bindings!"
}

# ----------------------------------------------------------
# Pool management
# ----------------------------------------------------------
Write-Host "Looking for application pool..."

$AppPoolPath = "IIS:\AppPools\" + $AppPoolName
if(Test-Path $AppPoolPath){
    Write-Host "AppPool with name" $AppPoolName "already exists."
}else{
    Write-Host "Creating new application pool.."
    New-WebAppPool $AppPoolName -Force
    Set-ItemProperty -Path $AppPoolPath -name "managedRuntimeVersion" -value "v4.0"
}

# ----------------------------------------------------------
# Site management
# ----------------------------------------------------------
Write-Host "Looking for website..."

$WebSitePath = "IIS:\Sites\" + $WebSiteName
$WebSitePhysicalPath = $IISAppsRootDirectory + $WebSiteName
if(Test-Path $WebSitePath){
    Write-Host "Website with name" $WebSiteName "already exists."
    Execute-WithRetry{
        Write-Host "Stopping website..." $WebSiteName
        Stop-Website -Name $WebSiteName
    }
    Write-Host "Website" $WebSiteName "stopped."
}else{
    Write-Host "Creating new website..."
    New-Item -Path "IIS:\Sites" -Name $WebSiteName -Type Site -Bindings @{protocol=$BindingProtocols[0];bindingInformation="*:"+$BindingPorts[0]+":"}
    Set-ItemProperty -Path $WebSitePath -name "applicationPool" -value $AppPoolName
    Set-ItemProperty -Path $WebSitePath -name "physicalPath" -value $WebSitePhysicalPath
    Set-ItemProperty -Path $AppPoolPath -name "managedRuntimeVersion" -value "v4.0"
}

    for ($i = 0; $i -lt $BindingProtocols.Count; $i++) {
        $binding = Get-WebBinding -Port $BindingPorts[$i] -Protocol $BindingProtocols[$i]
        if ($null -ne $binding)
        {
            Write-Host "Binding already exists for Port:" $BindingPorts[$i] "Protocol" $BindingProtocols[$i]
        }else{
            Write-Host "Setting binding - Port:" $BindingPorts[$i] "Protocol" $BindingProtocols[$i]
            $bindingInfo = "*:" + $BindingPorts[$i] + ":"
            New-ItemProperty -Path $WebSitePath -Name "bindings" -Value (@{protocol=$BindingProtocols[$i];bindingInformation=$bindingInfo})
        }
    }

# ----------------------------------------------------------
# Deploy application
# ----------------------------------------------------------

Write-Host "Extracting application files..."
#[System.IO.Compression.ZipFile]::ExtractToDirectory($PackagePath, $WebSitePhysicalPath)
try{
    Copy-Item $PackagePath"\*" $WebSitePhysicalPath -recurse -Force
} catch [System.Exception] {
    throw
}
Write-Host "Deployment successful..."

Execute-WithRetry{
    Write-Host "Starting website..." $WebSiteName
    Start-Website -Name $WebSiteName
}
Write-Host "Website" $WebSiteName "started."