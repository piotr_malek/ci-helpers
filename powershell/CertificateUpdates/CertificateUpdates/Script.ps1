﻿#
# Script.ps1
#
function Update-Dns([string] $nodeName, [string] $domain, [string] $data, [string] $clientId, [string] $secret){
	$url = "https://api.dynu.com/v1/oauth2/token"
	$base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $clientId,$secret)))
	$headers = @{
			Authorization=("Basic {0}" -f $base64AuthInfo)
		}
	$body = @{
			grant_type = "client_credentials"
		}

	$tokenRes = Invoke-RestMethod -Method 'Post' -Uri $url -ContentType 'application/x-www-form-urlencoded' -Headers $headers -Body $body -Verbose
	$accessToken = $tokenRes.accessToken

	$url = "https://api.dynu.com/v1/dns/records/$($domain)"
	$headers = @{
			Authorization=("Bearer {0}" -f $accessToken)
		}
	$listRecordsRes = Invoke-RestMethod -Method "Get" -Uri $url -Headers $headers -Verbose
	$domainRecords = @($listRecordsRes.SyncRoot).Where({$_.node_name -eq $nodeName})

	if($domainRecords.Count -gt 0){
		$record = $domainRecords[0]
		$recordId = $record.id

		$url = "https://api.dynu.com/v1/dns/record/delete/$($recordId)"
		$headers = @{
				Authorization=("Bearer {0}" -f $accessToken)
			}
		Invoke-RestMethod -Method "Get" -Uri $url -Headers $headers -Verbose
	}

	$url = "https://api.dynu.com/v1/dns/record/add"
	$headers = @{
			Authorization=("Bearer {0}" -f $accessToken)
		}
	$body = @{
		text_data = $data
		domain_name = $domain
		node_name = $nodeName
		record_type = "TXT"
		ttl = "90"
		state = "true"
	} | ConvertTo-Json
	$res = Invoke-RestMethod -Method 'Post' -Uri $url -ContentType 'application/json' -Headers $headers -Body $body -Verbose
}

Update-Dns -nodeName "_acme-challenge" -domain "sheeply.pl" -data "Ami pizza" -clientId "b131ff69-493e-4328-b20c-c827380440d2" -secret "YM6aYc7JYQhvpQJUKRM8cRKS9UJccS"